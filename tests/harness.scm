(define-module (tests-harness)
  #:use-module (srfi srfi-64)
  #:use-module (dummy))

(module-define! (resolve-module '(srfi srfi-64))
		'test-log-to-file #f)

(test-begin "test-suite")

(test-equal "Greetings"
  "Hi hackers !"
  (say-hi))

(test-equal "Goodbyes"
  "Bye bye hackers…"
  (say-bye))

(test-end "test-suite")
