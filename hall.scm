(hall-description
  (name "dummy")
  (prefix "guile")
  (version "0.1")
  (author "Red Nose Hacker")
  (copyright (2020))
  (synopsis "")
  (description "")
  (home-page "")
  (license gpl3+)
  (dependencies `())
  (files (libraries
           ((scheme-file "dummy") (directory "dummy" ())))
         (tests ((directory "tests" ((scheme-file "harness")))))
         (programs ((directory "scripts" ())))
         (documentation
           ((org-file "README")
            (symlink "README" "README.org")
            (text-file "HACKING")
            (text-file "COPYING")
            (directory "doc" ((texi-file "dummy")))))
         (infrastructure
           ((scheme-file "guix") (scheme-file "hall")))))
